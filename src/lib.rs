

#[cfg(not(feature = "pure_c"))]
#[allow(unused)]
pub mod ffi;
#[cfg(not(feature = "pure_c"))]
pub mod core;
#[cfg(not(feature = "pure_c"))]
pub mod recording;
#[cfg(not(feature = "pure_c"))]
pub mod database;
#[cfg(not(feature = "pure_c"))]
pub mod replacement;

/// Replaces rnd_; only module included in pure_c mode.
pub mod random;

#[link(name = "c_zork")]
extern "C" {
    /// Special entrypoint into the main function of Zork
    /// Only FFI definition in pure_c mode.
    pub fn c_main();
}