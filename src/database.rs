
use std::collections::HashMap;
use lazy_static::lazy_static;
use serde_json::Value;
use serde::Deserialize;

static DATABASE_RAW: &'static str = include_str!("../data/database.min.json");

lazy_static! {
    pub static ref DATABASE: Database = serde_json::from_str(DATABASE_RAW).unwrap();
}

trait ValueExt {
    fn as_i32(&self) -> Option<i32>;
}

impl ValueExt for Value {
    fn as_i32(&self) -> Option<i32> {
        self.as_i64().map(|i| i as i32)
    }
}

/// The (mostly) static data loaded from the database file.
#[derive(Deserialize)]
pub struct Database {
    pub version: Version,
    pub rooms: Vec<Room>,
    pub exit_travel: Vec<i32>,
    pub objects: Vec<ObjectEntry>,
    pub object_room_map: Vec<ObjectRoomPair>,
    /// This is modified by the game, so for now it's not exposed an API
    clock_events: Vec<ClockEvent>,
    pub villains: Vec<VillainEntry>,
    pub adventurers: Vec<AdventurerEntry>,
    pub star_base: i32,
    pub canned_messages: Vec<i32>,
    pub strings: HashMap<i32, String>
}

// Info about a particular adventurer.
#[derive(Deserialize)]
pub struct AdventurerEntry {
    // The current room the adventurer is in.
    pub room: i32,
    
    // Their score.
    pub score: i32,
    pub vehicle: i32,

    pub object: i32,
    pub action: i32,
    pub strength: i32,
    pub flags: i32
}

#[derive(Deserialize)]
pub struct Version {
    major: i32,
    minor: i32,
    /// Note that this is actually interpreted as a character
    edit: char
}

#[derive(Deserialize)]
pub struct Room {
    desc1: i32,
    desc2: i32,
    exit: i32,
    actions: i32,
    values: i32,
    flags: i32
}

#[derive(Deserialize)]
pub struct ObjectRoomPair {
    object: i32,
    room: i32,
}

#[derive(Deserialize)]
pub struct ClockEvent {
    tick: i32,
    action: i32,
    flags: i32
}

#[derive(Deserialize)]
pub struct VillainEntry {
    villains: i32,
    prob: i32,
    opps: i32,
    best: i32,
    melee: i32,
}

// All the metadata about one object.
#[derive(Deserialize)]
pub struct ObjectEntry {
    pub desc1: i32,
    pub desc2: i32,
    pub desco: i32,
    pub action: i32,
    pub flag1: i32,
    pub flag2: i32,
    pub fvalue: i32,
    pub tvalue: i32,

    // The object's size.
    pub size: i32,

    // The amount the object can hold, if it can hold anything.
    pub capacity: i32,

    // The room the object is in, or 0 if it isn't in a room.
    pub room: i32,

    // The adventurer that is holding this object, or 0 otherwise.
    pub adventurer: i32,

    // The container this object is in, or 0 otherwise.
    pub container: i32,
    pub read: i32
}

