

extern crate zork;

use std::fs::File;
use simplelog::{CombinedLogger, Config, LevelFilter, WriteLogger};
use log::trace;

fn main() {
    #[cfg(not(feature = "pure_c"))]
    {
        CombinedLogger::init(vec![
            WriteLogger::new(
                LevelFilter::Debug,
                Config::default(),
                File::create("log.txt").unwrap(),
            ),
            WriteLogger::new(
                LevelFilter::Trace,
                Config::default(),
                File::create("trace_log.txt").unwrap(),
            ),
        ]).unwrap();

        trace!("Starting c_main()");
    }

    unsafe {
        zork::c_main();
    }
}

// Even in pure_c mode, we still have to replace the rnd_ function.

