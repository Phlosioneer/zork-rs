// All the global state structures, defined in vars.h

use libc::c_int;
use log::{trace, error};
use super::Logical;
use crate::core;

pub use prsvec_ as parse_vec;
pub use play_ as player;
pub use advs_ as adventurers;
pub use last_ as last_it;

extern "C" {
    pub static mut prsvec_: ParseVec;
    //pub static mut orphs_: ?;
    pub static mut last_: c_int;
    pub static mut play_: Player;
    //pub static mut rooms_: RoomStates;
    //pub static mut rindex_: RoomIndecies?;
    //pub static mut xsrch_: ?;
    //pub static mut oroom2_: ?;
    //pub static mut oindex_: ?;
    //pub static mut cevent_: ?;
    //pub static mut cindex_: ?;
    pub static mut advs_: Adventurers;
    //pub static mut aflags_: c_int; // Actor Flags
    //pub static mut aindex_: ?; // Actor Index
    //pub static mut vindex_: ?; // Verb Index
    //pub static mut findex_: ?; // Flag index?
    //pub static mut debug_: ?; // Debug flags
    //pub static mut hack_: ?;
    //pub static mut vill_: ?; // Villians
    pub static mut state_: SaveState;
    //pub static mut curxt_: ?;
    //xpars_ are constants
    //star_ handled by ffi::objects
    //pub static mut input_: ?;
    //pub static mut screen_: ?;
    //pub static mut rmsg_: ?;
    pub static mut vers_: Version;
    pub static mut time_: GameTime;
    //pub static mut hyper_: c_int;
    //pub static mut exits_: ?;
    //pub static mut puzzle_: ?;
    //pub static mut bats_: ?;
}

// Info about the player.
#[repr(C)]
#[derive(Clone, Debug)]
pub struct Player {
    pub winner: c_int,
    pub current_room: c_int,
    pub tel_flag: Logical
}

// Info about all adventurers. (There are multiple...?)
#[repr(C)]
pub struct Adventurers {
    limit: c_int,
    
    // The current room for this adventurer.
    rooms: [c_int; 4],
    
    // The adventurer's score.
    scores: [c_int; 4],
    vehicles: [c_int; 4],
    
    // The current object referred to by the word "it".
    current_its: [c_int; 4],
    actions: [c_int; 4],
    strengths: [c_int; 4],
    flags: [c_int; 4]
}

// Info about a particular adventurer.
#[derive(Debug, Clone)]
pub struct AdventurerEntry {
    index: usize,
    
    // The current room the adventurer is in.
    pub room: c_int,
    
    // Their score.
    pub score: c_int,
    pub vehicle: c_int,

    // The current object referred to by the word "it".
    pub current_it: c_int,
    pub action: c_int,
    pub strength: c_int,
    pub flags: c_int
}

impl Adventurers {
    // Get an adventurer by their ID. Ids start at 1.
    pub fn get(&self, id: usize) -> AdventurerEntry {
        if id > self.len() {
            error!("Adventurer id {} is greater than object count ({}).", id, self.len());
            core::exit_program();
        }
        if id == 0 {
            error!("Adventurer id cannot be 0.");
            core::exit_program();
        }

        let index = id - 1;

        AdventurerEntry {
            index,
            room: self.rooms[index],
            score: self.scores[index],
            vehicle: self.vehicles[index],
            current_it: self.current_its[index],
            action: self.actions[index],
            strength: self.strengths[index],
            flags: self.flags[index]
        }
    }

    pub fn set(&mut self, values: &AdventurerEntry) {
        let index = values.index;

        self.rooms[index] = values.room;
        self.scores[index] = values.score;
        self.vehicles[index] = values.vehicle;
        self.current_its[index] = values.current_it;
        self.actions[index] = values.action;
        self.strengths[index] = values.strength;
        self.flags[index] = values.flags;
    }

    pub fn len(&self) -> usize {
        self.limit as usize
    }
}

impl AdventurerEntry {
    pub fn get_id(&self) -> usize {
        self.index + 1
    }
}

// A structure that stores info during parsing.
#[repr(C)]
#[derive(Clone, Debug)]
pub struct ParseVec {
    pub parse_a: c_int,
    pub parse_i: c_int,
    pub parse_o: c_int,
    pub parse_won: Logical,

    // Parsing should continue from this index in the parsing string. This is used
    // when an input string contains multiple commands (separated by '.' or ',').
    pub parse_continue: c_int,
}

#[repr(C)]
#[derive(Clone, Debug)]
pub struct SaveState {
    /// The number of inputs the player has made, regardless of whether
    /// they were valid or parsable.
    pub moves: c_int,
    /// The number of times the player has died.
    pub deaths: c_int,
    /// The player's score without counting the trophy case.
    pub raw_score: c_int,
    /// The maximum possible score in the game.
    pub max_score: c_int,
    /// The maximum weight the player can carry.
    pub max_weight: c_int,
    /// A one-time score the player can get from a "light shaft" room?
    pub light_shaft_score: c_int,
    /// The room index of the hot-air balloon.
    pub balloon_room: c_int,
    /// The room index of an NPC named "Mung".
    pub mung_room: c_int,
    
    pub hs: c_int,
    /// The player's endgame score
    pub endgame_score: c_int,
    /// The maximum possible score in the endgame.
    pub max_endgame_score: c_int,
}

// The version of the game.
#[repr(C)]
#[derive(Clone, Debug)]
pub struct Version {
    major: c_int,
    minor: c_int,
    /// Note that this is actually interpreted as a character
    edit: c_int
}

impl Version {
    fn major(&self) -> u8 {
        self.major as u8
    }

    fn minor(&self) -> u8 {
        self.minor as u8
    }

    fn edit(&self) -> char {
        (self.edit as u8) as char
    }
}

#[repr(C)]
#[derive(Clone, Debug)]
pub struct GameTime {
    /// The playtime before this session, in seconds
    previous_playtime: c_int,

    // Start time for this play session
    start_hour: c_int,
    start_minute: c_int,
    start_second: c_int,
}