
use libc::c_int;
use crate::database::DATABASE;
use crate::ffi::global_structs::player;
use log::trace;
use crate::ffi::Logical;
use std::io::{self, Write};

#[no_mangle]
pub extern "C" fn rspeak_(message_id: c_int) {
    trace!("rspeak_({})", message_id);
    rspeak_core(message_id, 0, 0, true);
}

#[no_mangle]
pub extern "C" fn rspsub_(message_id: c_int, substring_id: c_int) {
    trace!("rspsub_({}, {})", message_id, substring_id);
    rspeak_core(message_id, substring_id, 0, true);
}

#[no_mangle]
pub extern "C" fn rspsb2_(message_id: c_int, sub1_id: c_int, sub2_id: c_int) {
    trace!("rspsb2_({}, {}, {})", message_id, sub1_id, sub2_id);
    rspeak_core(message_id, sub1_id, sub2_id, true);
}

#[no_mangle]
pub extern "C" fn rspsb2nl_(message_id: c_int, sub1_id: c_int, sub2_id: c_int, newline: Logical) {
    trace!("rspsb2nl_({}, {}, {}, {:?})", message_id, sub1_id, sub2_id, newline);
    rspeak_core(message_id, sub1_id, sub2_id, newline.into());
}

fn rspeak_core(message_id: c_int, sub1_id: c_int, sub2_id: c_int, newline: bool) {
    if message_id != 0 {
        let message = resolve_string(message_id).unwrap();
        let sub1 = resolve_string(sub1_id);
        let sub2 = resolve_string(sub2_id);
        match (sub1, sub2) {
            (Some(sub1), Some(sub2)) => speak(message, &[sub1, sub2], newline.into()),
            (Some(sub), None) |
            (None, Some(sub)) => speak(message, &[sub], newline.into()),
            (None, None) => speak(message, &[], newline.into())
        }

        unsafe {
            player.tel_flag = true.into();
        }
    }
}

fn resolve_string(message_id: c_int) -> Option<&'static str> {
    if message_id > 0 {
        let indirect_id = DATABASE.canned_messages[(message_id - 1) as usize];
        trace!("Canned message id: {}", indirect_id);
        Some(&DATABASE.strings[&indirect_id])
    } else if message_id < 0 {
        Some(&DATABASE.strings[&message_id])
    } else {
        None
    }
}

fn speak(message: &str, substrings: &[&str], newline: bool) {
    trace!("speak({:?}, {:?})", message, substrings);
    
    let original_parts: Vec<_> = message.split('#').collect();
    
    for (i, part) in original_parts.into_iter().enumerate() {
        if i != 0 {
            print!("{}{}", substrings[i - 1], part);
        } else {
            print!("{}", part);
        }
    }
    if newline {
        println!("");
    } else {
        // Try to flush but it's not an error if we fail
        let _ = io::stdout().flush();
    }
}