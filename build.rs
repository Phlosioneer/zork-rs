extern crate build_helper;
extern crate cc;
extern crate pkg_config;

use build_helper::Profile;
use build_helper::cargo::{manifest, features};
use cc::Build;
use std::fs;
use std::ffi::OsStr;

fn main() {
    // Copy the dtextc.dat file into the build directory.
    let mut data_file = manifest::dir();
    data_file.push("c_src/dtextc.dat");
    let mut dest = manifest::dir();
    match build_helper::profile() {
        Profile::Debug => dest.push("target/debug/dtextc.dat"),
        Profile::Release => dest.push("target/release/dtextc.dat"),
    }
    fs::copy(data_file, dest.clone()).unwrap();

    // Compile and link the original C executable as a library.
    let paths = fs::read_dir("./c_src/").unwrap();

    let zork_files: Vec<_> = paths.map(|entry| entry.unwrap().path()).collect();
    for file in &zork_files {
        if let Some(name) = file.file_name() {
            println!("cargo:rerun-if-changed=c_src/{}", name.to_str().unwrap());
        }
    }
    let c_files = zork_files.into_iter()
        .filter(|ref p| p.extension() == Some(OsStr::new("c")));
    
    let mut dest_string = "\"".to_string();
    dest_string.push_str(&dest.to_str().unwrap().replace('\\', "/"));
    dest_string.push_str("\"");
    let mut build = Build::new();
    build.files(c_files)
        .include("c_src")
        .cpp(false)
        .static_crt(true)
        //.define("ALLOW_GDT", None)        // Enables the built-in debugger
        .define("TEXTFILE", Some(dest_string.as_str()))
        //.flag("-Werror=implicit-function-declaration")
        .flag_if_supported("-Wno-parentheses")
        .flag_if_supported("-Wno-unused-parameter")
        .flag_if_supported("-Wno-unused-but-set-variable")
        .flag_if_supported("-Wno-missing-braces");

    println!("cargo:rerun-if-env-changed=CARGO_FEATURE_PURE_C");
    if features::enabled("pure_c") {
        println!("cargo:warning=Pure C mode enabled");
        build.define("AS_MINIMAL_RUST_LIB", None);
    } else {
        build.define("AS_RUST_LIB", None);
    }

    if build.get_compiler().is_like_msvc() {
        // Disable MSVC's syntax extensions so that __STDC__ == 1
        build.flag("/Za");

        // Disable the default multithreading library libcmt
        // TODO: This is a LINKER flag, and cc can't modify those.
        //build.flag("/NODEFAULTLIB:libcmt.lib");
    }

    // Check if we have access to ncurses.
    if pkg_config::probe_library("ncurses").is_ok() {
        // Sets the terminal-interaction lib to ncurses
        build.define("MORE_TERMINFO", None);
    }
    
    build.compile("c_zork");

    
}
