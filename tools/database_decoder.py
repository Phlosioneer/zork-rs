
# The database is a big-endian binary file, partially XOR-encrypted.
#
import struct
import sys
import json
import math

class Cursor:
    def __init__(self, buffer):
        self.buffer = buffer
        self.position = 0
    
    def next_byte(self):
        ret = int(self.buffer[self.position])
        self.position += 1
        return ret

    def next_int(self):
        assert self.position + 2 <= len(self.buffer)
        ret = struct.unpack(">h", self.buffer[self.position:self.position + 2])[0]
        self.position += 2
        return ret
    
    def int_array(self, count):
        ret = []
        for _ in range(count):
            ret.append(self.next_int())
        return ret

    def sparse_array(self, count):
        ret = [0] * count
        while True:
            if count < 255:
                index = self.next_byte()
                if index == 255:
                    break
            else:
                index = self.next_int()
                if index == -1:
                    break
                assert index >= 0
                assert index < count
            ret[index] = self.next_int()
        return ret
    
    def byte_array(self, count):
        ret = []
        for _ in range(count):
            ret.append(self.next_byte())
        return ret

def struct_array(cursor, fields):
    count = cursor.next_int()
    ret = [{} for _ in range(count)]
    for name, f_type in fields.items():
        if f_type == "int":
            values = cursor.int_array(count)
        elif f_type == "sparse":
            values = cursor.sparse_array(count)
        elif f_type == "byte":
            values = cursor.byte_array(count)
        else:
            raise Exception()
        for i in range(count):
            ret[i][name] = values[i]
        
    return ret

def main():
    file_path = sys.argv[1]
    with open(file_path, "rb") as f:
        buffer = f.read()
    full_file = decode(Cursor(buffer))

    with open("data/database.json", "w") as f:
        json.dump(full_file, f, indent="\t")
    with open("data/database.min.json", "w") as f:
        json.dump(full_file, f, separators= (',', ':'))

def decode(cursor):
    major = cursor.next_int()
    minor = cursor.next_int()
    edit = chr(cursor.next_int())

    max_score = cursor.next_int()
    strbit = cursor.next_int()
    max_endgame_score = cursor.next_int()
    
    rooms = struct_array(cursor, {
        "desc1": "int",
        "desc2": "int",
        "exit": "int",
        "actions": "sparse",
        "values": "sparse",
        "flags": "int"
    })
    
    exit_count = cursor.next_int()
    exit_travel = cursor.int_array(exit_count)

    objects = struct_array(cursor, {
        "desc1": "int",
        "desc2": "int",
        "desco": "sparse",
        "action": "sparse",
        "flag1": "int",
        "flag2": "sparse",
        "fvalue": "sparse",
        "tvalue": "sparse",
        "size": "int",
        "capacity": "sparse",
        "room": "int",
        "adventurer": "sparse",
        "container": "sparse",
        "read": "sparse"
    })

    object_room_map = struct_array(cursor, {
        "object": "int",
        "room": "int"
    })

    clock_events = struct_array(cursor, {
        "tick": "int",
        "action": "int",
        "flags": "byte"
    })

    villains = struct_array(cursor, {
        "villains": "int",
        "prob": "sparse",
        "opps": "sparse",
        "best": "int",
        "melee": "int"
    })

    adventurers = struct_array(cursor, {
        "room": "int",
        "score": "sparse",
        "vehicle": "sparse",
        "object": "int",
        "action": "int",
        "strength": "int",
        "flags": "sparse"
    })

    star_base = cursor.next_int()
    message_count = cursor.next_int()
    canned_messages = cursor.int_array(message_count)
    encrypted_text = cursor.buffer[cursor.position:]

    # Text is encryted using a simple XOR cipher.
    message_encryption_key = "IanLanceTaylorJr"
    decrypted_message_body = ""
    for x in range(len(encrypted_text)):
        key = ord(message_encryption_key[x % len(message_encryption_key)])
        key ^= x % 256
        decrypted_message_body += chr(encrypted_text[x] ^ key)
    
    # Search for all the strings in the message body
    index = 1
    strings = {}
    while index * 8 < len(decrypted_message_body):
        # Find the corresponding offset
        offset = (index - 1) * 8

        # Extract the string
        string_end = decrypted_message_body.find("\0", offset)
        #assert string_end >= 0
        if string_end >= 0:
            string = decrypted_message_body[offset:string_end]
        else:
            string = decrypted_message_body[offset:]

        strings[str(-index)] = string

        # Advance the index to the next available slot
        # The +1 is to count the null character at the end of the string.
        index += math.ceil((len(string) + 1) / 8)


    return {
        "version": {
            "major": major,
            "minor": minor,
            "edit": edit
        },
        "rooms": rooms,
        "exit_travel": exit_travel,
        "objects": objects,
        "object_room_map": object_room_map,
        "clock_events": clock_events,
        "villains": villains,
        "adventurers": adventurers,
        "star_base": star_base,
        "canned_messages": canned_messages,
        "strings": strings
    }

if __name__ == "__main__":
    main()